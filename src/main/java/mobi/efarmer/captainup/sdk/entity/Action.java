package mobi.efarmer.captainup.sdk.entity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Maxim Maximchuk
 *         date 06.11.2014.
 */
public class Action {

    private String userId;

    private String name;
    private Float points;
    private Float pointsMultiplier;
    private Date date = new Date();

    private Map<String, String> entityMap = new HashMap<String, String>();

    public Action(String name, Player player) {
        this.name = name;
        this.userId = player.getId();
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPoints(Float points) {
        this.points = points;
    }

    public void setPointsMultiplier(Float pointsMultiplier) {
        this.pointsMultiplier = pointsMultiplier;
    }

    public void addEntityAttribute(String name, Object value) {
        entityMap.put(name, value.toString());
    }

    public Map<String, Object> toFormParams() {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("user", userId);
        params.put("action[name]", name);
        if (points != null) {
            params.put("action[points]", points.toString());
        }
        if (pointsMultiplier != null) {
            params.put("action[points_multiplier]", pointsMultiplier.toString());
        }
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        params.put("action[timestamp]", dateFormat.format(date));
        for (Map.Entry<String, String> entry: entityMap.entrySet()) {
            params.put("action[entity][" + entry.getKey() + "]", entry.getValue());
        }
        return params;
    }
}
