package mobi.efarmer.captainup.sdk;

/**
 * @author Maxim Maximchuk
 *         date 04.12.2014.
 */
public enum LeaderboardType {

    ALLTIME, MONTHLY, WEEKLY, DAILY;

    @Override
    public String toString() {
        String str = "";
        switch (this) {
            case ALLTIME: str = "all_time_ranking"; break;
            case MONTHLY: str = "monthly_ranking"; break;
            case WEEKLY: str = "weekly_ranking"; break;
            case DAILY: str = "daily_ranking"; break;
        }
        return str;
    }

}
