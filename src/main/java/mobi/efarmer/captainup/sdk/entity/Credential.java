package mobi.efarmer.captainup.sdk.entity;

/**
 * @author Maxim Maximchuk
 *         date 07.11.2014.
 */
public class Credential {

    private String appId;
    private String secret;

    public Credential(String appId, String secret) {
        this.appId = appId;
        this.secret = secret;
    }

    public String getAppId() {
        return appId;
    }

    public String getSecret() {
        return secret;
    }

}
