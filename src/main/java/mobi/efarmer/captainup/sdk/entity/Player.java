package mobi.efarmer.captainup.sdk.entity;

import com.maximchuk.json.JsonDTO;
import com.maximchuk.json.annotation.JsonParam;
import com.maximchuk.json.exception.JsonException;
import org.json.JSONObject;

import java.util.List;

/**
 * @author Maxim Maximchuk
 *         date 07.11.2014.
 */
public class Player extends JsonDTO {

    private String id;

    @JsonParam(name = "name")
    private String name;

    @JsonParam(name = "display_name")
    private String displayName;

    @JsonParam(name = "image")
    private String imageUrl;

    private Integer points;

    @JsonParam(name = "badges")
    private List<String> badgesIds;

    public Player() {
        super();
    }

    public Player(JSONObject json) throws JsonException {
        super(json);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public List<String> getBadgesIds() {
        return badgesIds;
    }

    public void setBadgesIds(List<String> badgesIds) {
        this.badgesIds = badgesIds;
    }
}
