package mobi.efarmer.captainup.sdk;

import com.maximchuk.json.JsonDTO;

/**
 * @author Maxim Maximchuk
 *         date 04.12.2014.
 */
public class TestLevel extends JsonDTO {

    public String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
