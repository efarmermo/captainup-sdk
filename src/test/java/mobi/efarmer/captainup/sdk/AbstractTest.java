package mobi.efarmer.captainup.sdk;

import mobi.efarmer.captainup.sdk.entity.Credential;

/**
 * @author Maxim Maximchuk
 *         date 07.11.2014.
 */
public abstract class AbstractTest {

    public static final String APP_ID = "54509bf773873a39ab0000b6";
    public static final String SECRET = "914e10b93aa74072806468b6e3a6c0c7ea3295c2f2d8ad816f888c18b599399d";

    private final Credential credential = new Credential(APP_ID, SECRET);

    public Credential getCredential() {
        return credential;
    }
}
