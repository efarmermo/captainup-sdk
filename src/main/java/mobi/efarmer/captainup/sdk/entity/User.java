package mobi.efarmer.captainup.sdk.entity;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Maxim Maximchuk
 *         date 07.11.2014.
 */
public class User {

    private String uri;
    private String name;
    private String firstName;
    private String lastName;
    private String imageUrl;
    private String email;

    public User(String uri) {
        this.uri = uri;
    }

    public User(Long id) {
        this.uri = String.valueOf(id);
    }

    public String getUri() {
        return uri;
    }

    public Long getId() {
        try {
            return Long.parseLong(uri);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Map<String, Object> toFormParams() {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("user[id]", uri);
        if (name != null) {
            params.put("user[name]", name);
        }
        if (imageUrl != null) {
            params.put("user[image]", imageUrl);
        }
        if (firstName != null) {
            params.put("user[first_name]", firstName);
        }
        if (lastName != null) {
            params.put("user[last_name]", lastName);
        }
        if (email != null) {
            params.put("user[email]", email);
        }
        return params;
    }
}
