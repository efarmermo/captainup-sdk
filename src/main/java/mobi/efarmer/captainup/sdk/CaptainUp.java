package mobi.efarmer.captainup.sdk;

import com.maximchuk.json.JsonDTO;
import com.maximchuk.json.exception.JsonException;
import com.maximchuk.rest.client.core.AbstractClient;
import com.maximchuk.rest.client.core.RestApiMethod;
import com.maximchuk.rest.client.http.HttpException;
import mobi.efarmer.captainup.sdk.entity.Action;
import mobi.efarmer.captainup.sdk.entity.Credential;
import mobi.efarmer.captainup.sdk.entity.Player;
import mobi.efarmer.captainup.sdk.entity.User;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Maxim Maximchuk
 *         date 07.11.2014.
 */
public class CaptainUp extends AbstractClient {

    private static final String SERVICE_URL = "https://captainup.com";
    private static final String CONTROLLER_NAME = "mechanics/v1";

    private Credential credential;
    private JSONObject appData;

    private int timeout = 10000;

    public CaptainUp(Credential credential) {
        this.credential = credential;
    }

    public JSONObject getAppData() throws IOException, HttpException {
        if (appData == null) {
            RestApiMethod method = new RestApiMethod("app/" + credential.getAppId(), RestApiMethod.Type.GET);
            method.setTimeout(timeout);
            appData = new JSONObject(executeMethod(method)).getJSONObject("data");
        }
        return appData;
    }

    public String sendAction(Action action) throws IOException, HttpException {
        RestApiMethod method = new RestApiMethod("actions", RestApiMethod.Type.POST);
        method.setTimeout(timeout);
        method.setParams(action.toFormParams());
        method.putParam("app", credential.getAppId());
        method.putParam("secret", credential.getSecret());
        return executeMethod(method);
    }

    public <T extends JsonDTO> T sendAction(Action action, T responseDto) throws Exception {
        responseDto.settingFromJson(new JSONObject(sendAction(action)));
        return responseDto;
    }

    public Player userConnect(User user) throws IOException, HttpException, JsonException {
        RestApiMethod method = new RestApiMethod("app/" + credential.getAppId() + "/players", RestApiMethod.Type.POST);
        method.setTimeout(timeout);
        method.setParams(user.toFormParams());
        method.putParam("secret", credential.getSecret());
        return new Player(new JSONObject(executeMethod(method)).getJSONObject("data"));
    }

    public String getPlayerInfo(String playerId) throws IOException, HttpException {
        RestApiMethod method = new RestApiMethod("players/" + playerId, RestApiMethod.Type.GET);
        method.setTimeout(timeout);
        method.putParam("app", credential.getAppId());
        return executeMethod(method);
    }

    public <T extends JsonDTO> T getPlayerInfo(Class<T> dtoClass, String playerId) throws Exception {
        JSONObject json = new JSONObject(getPlayerInfo(playerId));
        T dto = instantiateDto(dtoClass);
        dto.settingFromJson(json.getJSONObject("data"));
        return dto;
    }

    public JSONObject getBadgeJson(String badgeId) throws IOException, HttpException {
        JSONArray badges = getAppData().getJSONArray("badges");
        JSONObject badge = null;
        for (int i = 0; i < badges.length(); i++) {
            badge = badges.getJSONObject(i);
            if (badgeId.equals(badge.getString("_id"))) {
                break;
            }
        }
        return badge;
    }

    public String getBadge(String badgeId) throws IOException, HttpException {
        JSONObject badge = getBadgeJson(badgeId);
        return badge != null? badge.toString(): null;
    }

    public <T extends JsonDTO> T getBadge(Class<T> dtoClass, String badgeId) throws Exception {
        JSONObject badge = getBadgeJson(badgeId);
        T dto = null;
        if (badge != null) {
            dto = instantiateDto(dtoClass);
            dto.settingFromJson(badge);
        }
        return dto;
    }

    public String getBadges(Player player) throws IOException, HttpException {
        JSONArray jsonArray = new JSONArray();
        for (String badgeId: player.getBadgesIds()) {
            jsonArray.put(getBadge(badgeId));
        }
        return jsonArray.toString();
    }

    public <T extends JsonDTO> List<T> getBadges(Class<T> dtoClass, Player player) throws Exception {
        List<T> badges = new ArrayList<T>();
        for (String badgeId: player.getBadgesIds()) {
            badges.add(getBadge(dtoClass, badgeId));
        }
        return badges;
    }

    public <T extends JsonDTO> List<T> getLeaderBoard(Class<T> dtoClass, LeaderboardType type, int limit) throws Exception {
        return prepareDtoList(dtoClass, new JSONObject(getLeaderBoard(type, limit)).getJSONArray("data"));
    }

    public <T extends JsonDTO> List<T> getLeaderBoard(Class<T> dtoClass, LeaderboardType type, int skip, int limit) throws Exception {
        return prepareDtoList(dtoClass, new JSONObject(getLeaderBoard(type, skip, limit)).getJSONArray("data"));
    }

    public <T extends JsonDTO> List<T> getLeaderBoard(Class<T> dtoClass, LeaderboardType type, Player player, int limit) throws Exception {
        return prepareDtoList(dtoClass, new JSONObject(getLeaderBoard(type, player, limit)).getJSONArray("data"));
    }

    public String getLeaderBoard(LeaderboardType type, int limit) throws IOException, HttpException {
        return getLeaderBoard(type, null, limit, null);
    }

    public String getLeaderBoard(LeaderboardType type, int skip, int limit) throws IOException, HttpException {
        return getLeaderBoard(type, skip, limit, null);
    }

    public String getLeaderBoard(LeaderboardType type, Player player, int limit) throws IOException, HttpException {
        return getLeaderBoard(type, null, limit, player.getId());
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    private String getLeaderBoard(LeaderboardType type, Integer skip, Integer limit, String playerId)
            throws IOException, HttpException {
        String methodName = buildGetMethodName("leaderboards/") + type.toString();
        RestApiMethod method = new RestApiMethod(methodName, RestApiMethod.Type.GET);
        method.setTimeout(timeout);
        if (skip != null) {
            method.putParam("skip", skip.toString());
        }
        if (limit != null) {
            method.putParam("limit", limit.toString());
        }
        if (playerId != null) {
            method.putParam("player_id", playerId);
        }
        return executeMethod(method);
    }

    private <T extends JsonDTO> List<T> prepareDtoList(Class<T> dtoClass, JSONArray jsonArray) throws Exception {
        List<T> table = new ArrayList<T>();
        for (int i = 0; i < jsonArray.length(); i++) {
            T dto = instantiateDto(dtoClass);
            dto.settingFromJson(jsonArray.getJSONObject(i));
            table.add(dto);
        }
        return table;
    }

    private <T extends JsonDTO> T instantiateDto(Class<T> dtoClass) throws Exception {
        try {
            return dtoClass.newInstance();
        } catch (InstantiationException e) {
            throw new Exception("dtoClass must have default constructor");
        }
    }

    private String buildGetMethodName(String baseName) {
        return "app" + "/" + credential.getAppId() + "/" + baseName;
    }

    @Override
    protected String getServiceUrl() {
        return SERVICE_URL;
    }

    @Override
    protected String getControllerName() {
        return CONTROLLER_NAME;
    }
}
