package mobi.efarmer.captainup.sdk;

import com.maximchuk.json.JsonDTO;
import com.maximchuk.json.annotation.JsonParam;

/**
 * @author Maxim Maximchuk
 *         date 09.12.2014.
 */
public class TestBadge extends JsonDTO {

    @JsonParam(name = "_id")
    private String id;

    private String name;
    private Integer points;

    @JsonParam(name = "preset_image")
    private String imageUrl;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
