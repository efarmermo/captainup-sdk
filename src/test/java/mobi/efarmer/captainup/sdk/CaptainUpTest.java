package mobi.efarmer.captainup.sdk;

import com.maximchuk.json.JsonDTO;
import com.maximchuk.json.annotation.JsonParam;
import mobi.efarmer.captainup.sdk.entity.Action;
import mobi.efarmer.captainup.sdk.entity.Player;
import mobi.efarmer.captainup.sdk.entity.User;
import org.json.JSONObject;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class CaptainUpTest extends AbstractTest {

    private static final String TEST_USER_URI = "content://USER/7e65a639-c444-492b-bb21-6daa0a4d2f66";
    private static final long TEST_USER_ID = 19l;
    public static final String TEST_PLAYER_ID = "12872331141578094267090255910";

    private Action action;

    @Test
    public void testAppData() throws Exception {
        try {
            CaptainUp captainUp = new CaptainUp(getCredential());
            JSONObject resp = captainUp.getAppData();
            assertNotNull(resp);
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testUserConnect() {
        try {
            CaptainUp captainUp = new CaptainUp(getCredential());

            // test connect user by string uri
            Player playerByUri = captainUp.userConnect(new User(TEST_USER_URI));
            assertNotNull(playerByUri);

            //test connect user by long id
            Player playerById = captainUp.userConnect(new User(TEST_USER_ID));
            assertNotNull(playerById);
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }


    @Test
    public void testSendAction() {
        try {
            CaptainUp captainUp = new CaptainUp(getCredential());
            Player player = captainUp.userConnect(new User(TEST_USER_ID));

            action = new Action("fillprofile", player);
            action.addEntityAttribute("all_fields", true);

            String resp = captainUp.sendAction(action);
            assertNotNull(resp);

            TestStatus respObj = captainUp.sendAction(action, new TestStatus());
            assertNotNull(respObj);
            assertNotNull(respObj.basePoints);
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testGetPlayerInfo() throws Exception {
        try {
            CaptainUp captainUp = new CaptainUp(getCredential());
            String resp = captainUp.getPlayerInfo(TEST_PLAYER_ID);
            assertNotNull(resp);
            Player respDto = captainUp.getPlayerInfo(Player.class, TEST_PLAYER_ID);
            assertNotNull(respDto);
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testGetBadge() throws Exception {
        try {
            CaptainUp captainUp = new CaptainUp(getCredential());
            Player player = captainUp.getPlayerInfo(Player.class, TEST_PLAYER_ID);
            String resp = captainUp.getBadge(player.getBadgesIds().get(0));
            assertNotNull(resp);
            TestBadge respDto = captainUp.getBadge(TestBadge.class, player.getBadgesIds().get(0));
            assertNotNull(respDto);
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testGetBadges() throws Exception {
        try {
            CaptainUp captainUp = new CaptainUp(getCredential());
            Player player = captainUp.getPlayerInfo(Player.class, TEST_PLAYER_ID);
            String resp = captainUp.getBadges(player);
            assertNotNull(resp);
            List<TestBadge> respDto = captainUp.getBadges(TestBadge.class, player);
            assertNotNull(respDto);
        } catch (Exception e) {
            fail(e.getMessage());
        }

    }

    @Test
    public void testGetLeaderBoard() throws Exception {
        try {
            CaptainUp captainUp = new CaptainUp(getCredential());
            String resp = captainUp.getLeaderBoard(LeaderboardType.ALLTIME, 10);
            assertNotNull(resp);

            Player player = captainUp.userConnect(new User(TEST_USER_URI));

            // All time
            List<TestLeaderboardItem> allTimeleaderBoard = captainUp.getLeaderBoard(TestLeaderboardItem.class, LeaderboardType.ALLTIME, player, 11);
            assertFalse(allTimeleaderBoard.isEmpty());

            // weekly
            List<TestLeaderboardItem> weeklyLeaderBoard = captainUp.getLeaderBoard(TestLeaderboardItem.class, LeaderboardType.WEEKLY, player, 11);
            assertFalse(weeklyLeaderBoard.isEmpty());
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    public class TestStatus extends JsonDTO {

        public Float points;

        @JsonParam(name = "base_points")
        public Float basePoints;

        public Float getPoints() {
            return points;
        }

        public void setPoints(Float points) {
            this.points = points;
        }

        public Float getBasePoints() {
            return basePoints;
        }

        public void setBasePoints(Float basePoints) {
            this.basePoints = basePoints;
        }
    }

}