package mobi.efarmer.captainup.sdk;

import com.maximchuk.json.JsonDTO;
import com.maximchuk.json.annotation.JsonParam;

/**
 * @author Maxim Maximchuk
 *         date 04.12.2014.
 */
public class TestLeaderboardItem extends JsonDTO {

    @JsonParam(name = "id")
    public String playerId;

    public String name;

    @JsonParam(name = "image")
    public String imageUrl;

    public Float points;

    @JsonParam(name = "weekly_points")
    public Float weeklyPoints;

    @JsonParam(name = "all_time_position")
    public Integer position;

    @JsonParam(name = "weekly_position")
    public Integer weeklyPosition;


    public TestLevel level;

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Float getPoints() {
        return points;
    }

    public void setPoints(Float points) {
        this.points = points;
    }

    public Float getWeeklyPoints() {
        return weeklyPoints;
    }

    public void setWeeklyPoints(Float weeklyPoints) {
        this.weeklyPoints = weeklyPoints;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Integer getWeeklyPosition() {
        return weeklyPosition;
    }

    public void setWeeklyPosition(Integer weeklyPosition) {
        this.weeklyPosition = weeklyPosition;
    }

    public TestLevel getLevel() {
        return level;
    }

    public void setLevel(TestLevel level) {
        this.level = level;
    }
}
